LNP-scraper a.k.a. Łączy Nas Piłka - scraper 
=============================================
A simple script which scraps page content from given set of urls on laczynaspilka.pl site.

# Getting started

### Create virtual environment
```bash
python3 -m venv .venv
```

### Activate virtual environment
```bash
source .venv/bin/activate
```

### Install required packages
```bash
pip install -r requirements.txt
```

### Run scraper
```bash
python scraper2.py
```

------

### See also
* [The Python Code: "How to Use Proxies to Rotate IP Addresses in Python"](https://www.thepythoncode.com/article/using-proxies-using-requests-in-python)
