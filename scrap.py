import requests
from proxybroker import Broker
import asyncio
import random
from bs4 import BeautifulSoup
import time

from player_urls import urls


# get n proxies from proxybroker
def getProxies(n):
    async def show(proxies):
        p = []
        while True:
            proxy = await proxies.get()
            if proxy is None:
                break
            p.append("{}://{}:{}".format(proxy.schemes[0].lower(), proxy.host, proxy.port))
        return p

    proxies = asyncio.Queue()
    broker = Broker(proxies)
    tasks = asyncio.gather(broker.find(types=['HTTP', 'HTTPS'], limit=n), show(proxies))
    loop = asyncio.get_event_loop()
    return loop.run_until_complete(tasks)[1]


def main():
    proxyPool = getProxies(50)
    random.shuffle(proxyPool)
    print('start!')
    for url in urls:
        t = random.randint(1, 4)
        time.sleep(t)
        i = 0
        for proxy in proxyPool:
            try:
                qty = "dupsko"
                response = requests.get(url, proxies={"http": proxy, "https": proxy})
                if response.status_code == 200:
                    soup = BeautifulSoup(response.content, "html5lib")
                    try:
                        qty = soup.findAll('span', {'class': 'qty'})[1]
                    except Exception:
                        i += 1
                        print(i)
                        continue
                    print('%d | QTY === %s | %s' % (i, qty, url))
                    i = 0
                    break
            except requests.exceptions.ProxyError:
                i += 1
                print(i)
                raise
            except Exception:
                print(i)
                i += 1
                raise


if __name__ == '__main__':
    main()
