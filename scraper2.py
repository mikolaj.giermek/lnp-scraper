from urllib.request import Request, urlopen
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
import random
import requests

from player_urls import urls

ua = UserAgent() # From here we generate a random user agent
proxies = [] # Will contain proxies [ip, port]

# Main function
def main():
    # Retrieve latest proxies
    print('start')
    proxies_req = Request('https://www.sslproxies.org/')
    proxies_req.add_header('User-Agent', ua.random)
    proxies_doc = urlopen(proxies_req).read().decode('utf8')

    soup = BeautifulSoup(proxies_doc, 'html5lib')
    proxies_table = soup.find(id='proxylisttable')

    # Save proxies in the array
    for row in proxies_table.tbody.find_all('tr'):
        proxies.append({
            'ip':   row.find_all('td')[0].string,
            'port': row.find_all('td')[1].string
        })

    # Choose a random proxy
    proxy_index = random_proxy()
    proxy = proxies[proxy_index]
    for n, url in enumerate(urls):
        print(url)
        proxy_address = proxy['ip'] + ':' + proxy['port']
        # Every 10 requests, generate a new proxy
        if n % 10 == 0:
            proxy_index = random_proxy()
            proxy = proxies[proxy_index]
            print(proxy)

        # Make the call
        qty = ''
        while True:
            try:
                response = requests.get(
                    url, 
                    proxies={"http": proxy_address, "https": proxy_address},
                    headers={"User-Agent": ua.random},
                    timeout=30,
                )
                soup = BeautifulSoup(response.content, "html5lib")
                qty = soup.findAll('span', {'class': 'qty'})[1]
                print(qty)
                break
            except Exception as e:  # If error, delete this proxy and find another one
                print(e)
                del proxies[proxy_index]
                print('Proxy ' + proxy['ip'] + ':' + proxy['port'] + ' deleted.')
                proxy_index = random_proxy()
                proxy = proxies[proxy_index]
                print(proxy)

# Retrieve a random index proxy (we need the index to delete it if not working)
def random_proxy():
    return random.randint(0, len(proxies) - 1)

if __name__ == '__main__':
    main()